#from pyexpat import model
from tabnanny import verbose
from django.contrib.auth.models import AbstractUser
from django.db import models

class Usuario(AbstractUser):
    telefono =  models.IntegerField(null=True)
    rut = models.IntegerField(unique=True, null=True)
    email = models.EmailField('email address', unique = True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name', 'telefono', 'rut']

    #def __str__(self):
    #    return self.first_name + ' ' + self.last_name

class Membresia(models.Model):
    #lista_tipo = (
    #    ('Oro', 'Oro'),
    #)
    #tipo = models.CharField(max_length=20, choices=lista_tipo)
    tipo = models.CharField(max_length=50)
    def __str__(self):
        return self.tipo

class Bloque(models.Model):
    duracion = models.CharField(max_length=200)
    def __str__(self):
        return str(self.duracion)

class Actividad(models.Model):
    nombre_act = models.CharField(max_length=200)
    def __str__(self):
        return str(self.nombre_act) 
    
    class Meta:
        verbose_name_plural = 'Actividades'

class Profesor(models.Model):
    nombre_prof = models.CharField(max_length=200)
    apellido_prof = models.CharField(max_length=200)
    def __str__(self):
        return str(self.nombre_prof) + ' ' + str(self.apellido_prof)

class Horario(models.Model):
    fecha = models.DateField()
    fk_bloque = models.ForeignKey(to=Bloque, on_delete=models.CASCADE, null=True,verbose_name='Bloque')
    fk_actividad = models.ForeignKey(to=Actividad, on_delete=models.CASCADE, null=True, verbose_name='Actividad')
    fk_profesor = models.ForeignKey(to=Profesor, on_delete=models.CASCADE, null=True, verbose_name='Profesor')
    fk_usuario = models.ForeignKey(to=Usuario, blank=True,on_delete=models.CASCADE, null=True, verbose_name="Usuario")
    asistencia = models.BooleanField()

    def __str__(self):
        return str(self.fecha) + ' ' + str(self.fk_bloque.duracion) + ' ' + str(self.fk_actividad.nombre_act) + ' ' + str(self.fk_profesor.nombre_prof) + ' ' + str(self.fk_profesor.apellido_prof) +  ' ' + str(self.asistencia)
    

class historial(models.Model):
    membresia = models.ForeignKey(Membresia, on_delete=models.CASCADE)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    fecha_inicio = models.DateField()
    fecha_termino = models.DateField()
    estado = models.BooleanField()

    #class Meta:
    #    unique_together = ('membresia', 'usuario',)
    
    def __str__(self):
        return self.usuario.first_name + '-' + self.membresia.tipo + '-' + str(self.estado)

