from collections import UserString
from re import template
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.template import loader
from django.conf import settings
from django.contrib.auth.decorators import user_passes_test
from django.contrib import messages
from django.views.generic import View
from .models import Usuario, Horario, Bloque, Actividad, Profesor, historial, Membresia
from .forms import UsuarioCreationForm, HorarioCreationForm, ReservaCreationForm, CreacionUsuario, HistorialForm, RegisterForm, ReservaDeleteCreationForm
from django.db.models import Count
import json
from datetime import datetime
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import Group





def es_administrador(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo Cliente?
        return user.groups.filter(name='Administrador').exists()
    else:
        return False


# Decorador para verificar grupo.
def es_cliente(user):
    # usuario autenticado?
    if (user.is_authenticated):
        # usuario pertenece al grupo Cliente?
        return user.groups.filter(name='Cliente').exists()
    else:
        return False

# Create your views here.


@user_passes_test(es_administrador)
def horario(request):
    template = loader.get_template('gimnasio/administrador/horario.html')
    a = datetime.today().strftime('%Y-%m-%d')
    #horario = Horario.objects.filter(fecha="2022-06-13")
    horario = Horario.objects.all().filter(fecha__gte=a)
    context = {
        'horario': horario,
        'settings': settings
    }
    # print(ejemplares[0]["disponible"])
    # print(ejemplares[0]["disponible"])
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    return HttpResponse(template.render(context, request))


@user_passes_test(es_administrador)
def pasados(request):
    template = loader.get_template('gimnasio/administrador/horario_pasado.html')
    a = datetime.today().strftime('%Y-%m-%d')
    #horario = Horario.objects.filter(fecha="2022-06-13")
    horario = Horario.objects.all().filter(fecha__lte=a)
    context = {
        'horario': horario,
        'settings': settings
    }
    # print(ejemplares[0]["disponible"])
    # print(ejemplares[0]["disponible"])
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    return HttpResponse(template.render(context, request))


@user_passes_test(es_administrador)
def usuario(request):
    template = loader.get_template('gimnasio/administrador/usuario.html')

    context = {
        'settings': settings
    }
    # print(ejemplares[0]["disponible"])
    # print(ejemplares[0]["disponible"])
    #ejemplares = Ejemplar.objects.filter(libro=libros)

    return HttpResponse(template.render(context, request))

@user_passes_test(es_administrador)
def graficar(request):
    template = loader.get_template('gimnasio/administrador/graficos.html')
    context = {
        'settings': settings,
        'dataarticulos': None,
    }
    membresias = historial.objects.filter()
    print(membresias)
    result = (membresias
                .values('membresia_id')
                .annotate(dcount=Count('membresia_id'))
                  .order_by()
                )

    datamembresia = []
    for item in result:
        obj_rev = Membresia.objects.filter(
            id=item['membresia_id']).values()[0]
        datamembresia.append(
                {'label': obj_rev["tipo"], 'y': item["dcount"]})

    context['datamembresia'] = json.dumps(datamembresia)

    actividades = Horario.objects.filter()
    result = (actividades
                  .values('fk_actividad_id')
                  .annotate(dcount=Count('fk_actividad_id'))
                  .order_by()
                  )

    dataactividades = []
    for item in result:
        obj_rev = Actividad.objects.filter(
            id=item['fk_actividad_id']).values()[0]
        dataactividades.append(
                {'label': obj_rev["nombre_act"], 'y': item["dcount"]})

    context['dataactividades'] = json.dumps(dataactividades)
    
    
    actividades2 = Horario.objects.filter()
    result = (actividades2
                  .values('fk_profesor_id')
                  .annotate(dcount=Count('fk_profesor_id'))
                  .order_by()
                  )

    dataactividades2 = []
    for item in result:
        obj_rev = Profesor.objects.filter(
            id=item['fk_profesor_id']).values()[0]
        dataactividades2.append(
                {'label': obj_rev["nombre_prof"], 'y': item["dcount"]})
    context['dataactividades2'] = json.dumps(dataactividades2)
    
    bloques = Horario.objects.exclude(fk_usuario_id__isnull=True)
    print(bloques)
    result = (bloques
                  .values('fk_bloque_id')
                  .annotate(dcount=Count('fk_bloque_id'))
                  .order_by()
                  )

    bloques = []
    for item in result:
        obj_rev = Bloque.objects.filter(
            id=item['fk_bloque_id']).values()[0]
        bloques.append(
                {'label': obj_rev["duracion"], 'y': item["dcount"]})
    context['bloques'] = json.dumps(bloques)
    
    return HttpResponse(template.render(context, request))


@user_passes_test(es_administrador)
def index2(request):
    template = loader.get_template('gimnasio/administrador/index.html')
    context = {
        'settings': settings,
        'isadd': True,
    }

    return HttpResponse(template.render(context, request))

@user_passes_test(es_cliente)
def clienteindex(request):
    template = loader.get_template('gimnasio/cliente/index.html')
    membresia = historial.objects.filter(usuario_id =request.user.id)
    context = {
        'settings': settings,
        'isadd': True,
        'membresia': membresia,
    }

    return HttpResponse(template.render(context, request))


@user_passes_test(es_administrador)
def update(request, id):
    horar = Horario.objects.get(id=id)
    if (request.method == 'GET'):
        form = HorarioCreationForm(instance=horar)
        # muesta usuarios de esos grupos.
        form.fields['fk_bloque'].queryset = Bloque.objects.filter()
        form.fields['fk_actividad'].queryset = Actividad.objects.filter()
        form.fields['fk_profesor'].queryset = Profesor.objects.filter()
        form.fields['fk_usuario'].queryset = Usuario.objects.filter(
            groups__name__in=['Cliente'])
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])

        # mostrar formulario.
        template = loader.get_template('gimnasio/administrador/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = HorarioCreationForm(request.POST, instance=horar)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('../')


@user_passes_test(es_administrador)
def update2(request, id):
    horar = Horario.objects.get(id=id)
    if (request.method == 'GET'):
        form = HorarioCreationForm(instance=horar)
        # muesta usuarios de esos grupos.
        form.fields['fk_bloque'].queryset = Bloque.objects.filter()
        form.fields['fk_actividad'].queryset = Actividad.objects.filter()
        form.fields['fk_profesor'].queryset = Profesor.objects.filter()
        form.fields['fk_usuario'].queryset = Usuario.objects.filter(
            groups__name__in=['Cliente'])
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])

        # mostrar formulario.
        template = loader.get_template('gimnasio/administrador/update2.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = HorarioCreationForm(request.POST, instance=horar)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('/gimnasio/administrador/reservashoy/')
    
    
@user_passes_test(es_administrador)
def delete(request, id):
    horar = Horario.objects.get(id=id)
    
    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('gimnasio/administrador/delete.html')

        context = {
            'settings': settings,
            'horar': horar,
        }
    
        return HttpResponse(template.render(context, request))
        
    elif (request.method == 'POST'):
        # elimina la publicación.
        horar.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('../')
    

@user_passes_test(es_administrador)
def updateusuario(request, id):
    usuario = Usuario.objects.get(id=id)
    if (request.method == 'GET'):
        form = CreacionUsuario(instance=usuario)

        # mostrar formulario.
        template = loader.get_template('gimnasio/administrador/usuarioadd.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = CreacionUsuario(request.POST, instance=usuario)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('/gimnasio/administrador/usuariodashboard/')
    

@user_passes_test(es_administrador)
def deleteusuario(request, id):
    usuario = Usuario.objects.get(id=id)
    
    if (request.method == 'GET'):
        # muestra el formulario.
        template = loader.get_template('gimnasio/administrador/deleteusuario.html')

        context = {
            'settings': settings,
            'usuario': usuario,
        }
    
        return HttpResponse(template.render(context, request))
        
    elif (request.method == 'POST'):
        # elimina la publicación.
        usuario.delete()
        messages.success(request, "Eliminación realizada.")

        return redirect('/gimnasio/administrador/usuariodashboard/')
    
    
@user_passes_test(es_administrador)
def usuariodashboard(request):
    template = loader.get_template('gimnasio/administrador/usuariodashboard.html')
    usuarios = Usuario.objects.filter(
        groups__name__in=['Cliente'])
    context = {
            'settings': settings,
            'usuarios' : usuarios,
        }
    
    return HttpResponse(template.render(context, request))

@user_passes_test(es_administrador)
def usuarioadd(request):
    data = {
        'form': RegisterForm()
    }
    print(data)
    # Metodo de accion por POST
    if request.method == 'POST':
        # Asignamos el formulario con los datos ingresados
        formulario  = RegisterForm(data=request.POST)
        # Fomulario debe estar lleno
        if formulario.is_valid():
            # Guardamos al usuario con los datos del formulario
            user = formulario.save()
            # Obtenemos el grupo filtrando **en mi caso es cliente**
            my_group = Group.objects.get(name='Cliente')
            # Agregamos el grupo cliente al nuevo usuario
            my_group.user_set.add(user)
            # Lo logeamos 
            # No hace falta autenticacion pq nosotros le dimos el grupo
            #   login(request,user)
            # Error qlo no se pq pero lo vi en overflow owo
            if user is not None:
                # Mensaje de bienvenida
                messages.success(request,"Usuario creado, no olvidar de agregarle su membresía")
                # Mostramos mensaje por terminal
                print("El {} ha sido agregado al grupo {}".format(user,my_group))
                # Lo mando a la vista de clientes
                return redirect("/gimnasio/administrador/usuario")
            
        data["form"] = formulario
    # cargamos plantilla de registro
    return render(request,"registration/registro.html", data)
    
    
    """
    if (request.method == 'GET'):
        form = CreacionUsuario()

        template = loader.get_template('gimnasio/administrador/usuarioadd.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = CreacionUsuario(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('/gimnasio/administrador/usuario/')
"""

@user_passes_test(es_administrador)
def reservashoy(request):
    template = loader.get_template('gimnasio/administrador/reservashoy.html')
    a = datetime.today().strftime('%Y-%m-%d')

    
    horario = Horario.objects.filter(fecha=a)
    print(horario)
    context = {
        'settings': settings,
        'isadd': True,
        'horario':horario,
    }

    return HttpResponse(template.render(context, request))




@user_passes_test(es_administrador)
def usuariobuscar(request, id):
    template = loader.get_template('gimnasio/administrador/usuariobuscar.html')
    usuarios = Usuario.objects.filter(id=id)
    histo = historial.objects.filter(usuario=id)
    actividades = Horario.objects.filter(fk_usuario=id)
    toma_bloques = Horario.objects.filter(fk_usuario=id)
    
    context = {
            'settings': settings,
            'usuarios' : usuarios,
            'histo':histo,
            'actividades': actividades,
            'toma_bloques': toma_bloques,
        }
    
    membresias = historial.objects.filter(usuario=id)
    result = (membresias
                .values('membresia_id')
                .annotate(dcount=Count('membresia_id'))
                .order_by()
                )

    datamembresia = []
    for item in result:
        obj_rev = Membresia.objects.filter(
            id=item['membresia_id']).values()[0]
        datamembresia.append(
                {'label': obj_rev["tipo"], 'y': item["dcount"]})

    context['datamembresia'] = json.dumps(datamembresia)


    result = (actividades
                  .values('fk_actividad_id')
                  .annotate(dcount=Count('fk_actividad_id'))
                  .order_by()
                  )

    dataactividades = []
    for item in result:
        obj_rev = Actividad.objects.filter(
            id=item['fk_actividad_id']).values()[0]
        dataactividades.append(
                {'label': obj_rev["nombre_act"], 'y': item["dcount"]})

    context['dataactividades'] = json.dumps(dataactividades)
    
    result = (actividades
                  .values('fk_actividad_id')
                  .annotate(dcount=Count('fk_actividad_id'))
                  .order_by()
                  )

    dataactividades = []
    for item in result:
        obj_rev = Actividad.objects.filter(
            id=item['fk_actividad_id']).values()[0]
        dataactividades.append(
                {'label': obj_rev["nombre_act"], 'y': item["dcount"]})

    context['dataactividades'] = json.dumps(dataactividades)

    result = (toma_bloques
                  .values('fk_bloque_id')
                  .annotate(dcount=Count('fk_bloque_id'))
                  .order_by()
                  )

    bloques = []
    for item in result:
        obj_rev = Bloque.objects.filter(
            id=item['fk_bloque_id']).values()[0]
        bloques.append(
                {'label': obj_rev["duracion"], 'y': item["dcount"]})
    context['bloques'] = json.dumps(bloques)
    
    
    
    
    return HttpResponse(template.render(context, request))


@user_passes_test(es_administrador)
def add(request):
    if (request.method == 'GET'):
        form = HorarioCreationForm()
        # muesta usuarios de esos grupos.
        form.fields['fk_bloque'].queryset = Bloque.objects.filter()
        form.fields['fk_actividad'].queryset = Actividad.objects.filter()
        form.fields['fk_profesor'].queryset = Profesor.objects.filter()
        form.fields['fk_usuario'].queryset = Usuario.objects.filter(
            groups__name__in=['Cliente'])
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])

        # mostrar formulario.
        template = loader.get_template('gimnasio/administrador/add.html')
        horario = Horario.objects.all()

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = HorarioCreationForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('../')


@user_passes_test(es_administrador)
def change_historial2(request, id):
    if (request.method == 'GET'):
        form = HistorialForm()
        # muesta usuarios de esos grupos.
        form.fields['membresia'].queryset = Membresia.objects.filter()
        form.fields['usuario'].queryset = Usuario.objects.filter(
            id=id)
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        # mostrar formulario.
        template = loader.get_template('gimnasio/administrador/historial.html')
        hist = historial.objects.all()

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = HistorialForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('/gimnasio/administrador/index')


@user_passes_test(es_administrador)
def change_historial3(request, id):
    if (request.method == 'GET'):
        form = HistorialForm()
        # muesta usuarios de esos grupos.
        form.fields['membresia'].queryset = Membresia.objects.filter()
        form.fields['usuario'].queryset = Usuario.objects.filter(
            id=id)
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        # mostrar formulario.
        template = loader.get_template('gimnasio/administrador/historial.html')
        hist = historial.objects.all()

        context = {
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = HistorialForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('/gimnasio/administrador/index')


@user_passes_test(es_administrador)
def change_historial(request):
    if (request.method == 'GET'):
        form = HistorialForm()
        # muesta usuarios de esos grupos.
        form.fields['membresia'].queryset = Membresia.objects.filter()
        form.fields['usuario'].queryset = Usuario.objects.filter(
            groups__name__in = ['Cliente'])
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])
        # mostrar formulario.
        template = loader.get_template('gimnasio/administrador/historial.html')
        hist = historial.objects.all()

        context = {
            'settings': settings,
            'form': form,
            'isadd': True,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = HistorialForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, "Ingreso realizado.")

        return redirect('/gimnasio/administrador/index')


@user_passes_test(es_cliente)
def index(request):
    template = loader.get_template('cliente/index.html')
    context = {
        'settings': settings,
        'isadd': True,
    }

    return HttpResponse(template.render(context, request))


"""
@user_passes_test(es_admin)
def horario(request):
    template = loader.get_template('horario/horario.html')
    context = {

    }

    return HttpResponse(template.render(context, request))
"""


@user_passes_test(es_cliente)
def musculacion(request):
    usur = historial.objects.filter(usuario=request.user.id).filter(estado=True)
    template = loader.get_template('gimnasio/musculacion/index.html')
    context = {
        'usur': usur,
    }

    return HttpResponse(template.render(context, request))


@user_passes_test(es_cliente)
def reserva(request):
    a = datetime.today().strftime('%Y-%m-%d')
    template = loader.get_template('gimnasio/reserva/reserva.html')
    print(a)
    """
    Plugin de calendario
    Filtrar por fecha actual, tomar fecha con python
    poder cancelar una reserva
    """
    #horario = Horario.objects.all()
    #print(horario[4])
    
    horario = Horario.objects.all().filter(fk_usuario_id__isnull=True).filter(fecha__gte = a)
    usur = historial.objects.filter(usuario=request.user.id).filter(estado=True)
    print(horario)
    context = {
        'horario': horario,
        'settings': settings,
        'usur': usur,
    }

    return HttpResponse(template.render(context, request))



@user_passes_test(es_cliente)
def membresia(request):
    if (request.method == 'GET'):
        # mostrar formulario.
        template = loader.get_template('gimnasio/cliente/membresias.html')
        membresia = historial.objects.filter(usuario_id =request.user.id)
        usur = historial.objects.filter(usuario=request.user.id).filter(estado=True)
        print(membresia)
        context = {
            'settings': settings,
            'membresia': membresia,
            'usur':usur,
        }

    return HttpResponse(template.render(context, request))

@user_passes_test(es_cliente)
def historialhorario(request):
    a = datetime.today().strftime('%Y-%m-%d')
    horario = Horario.objects.all().filter(asistencia=False).filter(fk_usuario=request.user.id).filter(fecha = a)
    print(horario)
    id_us = request.user.id
    usur = historial.objects.filter(usuario=request.user.id).filter(estado=True)
    print(horario)
    template = loader.get_template('gimnasio/cliente/historialhorario.html')
    
    context = {
            'settings': settings,
            'membresia': membresia,
            'horario': horario,
            'usur':usur,
        }

    return HttpResponse(template.render(context, request))

@user_passes_test(es_cliente)
def reserva_upd(request, id):
    horar = Horario.objects.get(id=id)
    field_name = 'fecha'
    field_name_val = getattr(horar, field_name)
    print(field_name_val)
    val = Horario.objects.all().filter(fk_usuario=request.user.id).filter(fecha=field_name_val)
    print(val)
    #horario2 = Horario.objects.filter(id=request.user.id).filter(fecha=)
    if (request.method == 'GET'):
        form = ReservaCreationForm(instance=horar)

        # muesta usuarios de esos grupos.
        form.fields['fk_bloque'].queryset = Bloque.objects.filter()
        form.fields['fk_actividad'].queryset = Actividad.objects.filter()
        form.fields['fk_profesor'].queryset = Profesor.objects.filter()
        form.fields['fk_usuario'].queryset = Usuario.objects.filter(id=request.user.id)
        #form.fields['fk_usuario'].queryset = Usuario.objects.filter()
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])

        # mostrar formulario.
        template = loader.get_template('gimnasio/reserva/add.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': False,
            'horar': horar,
            'val': val,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = ReservaCreationForm(request.POST, instance=horar)
        if form.is_valid():
            print("oli")
            form.save()
            messages.success(request, "Ingreso realizado.")
        
        return redirect('../')



def Registro(request):
    # Cargamos los fields desde forms.py
    data = {
        'form': RegisterForm()
    }
    # Metodo de accion por POST
    if request.method == 'POST':
        # Asignamos el formulario con los datos ingresados
        formulario  = RegisterForm(data=request.POST)
        # Fomulario debe estar lleno
        if formulario.is_valid():
            # Guardamos al usuario con los datos del formulario
            user = formulario.save()
            # Obtenemos el grupo filtrando **en mi caso es cliente**
            my_group = Group.objects.get(name='Cliente')
            # Agregamos el grupo cliente al nuevo usuario
            my_group.user_set.add(user)
            # Lo logeamos 
            # No hace falta autenticacion pq nosotros le dimos el grupo
            login(request,user)
            # Error qlo no se pq pero lo vi en overflow owo
            if user is not None:
                # Mensaje de bienvenida
                messages.success(request,"Si desea entrar a todas las funciones de este menú, contactese con el administrador para que le habilite su membresía")
                # Mostramos mensaje por terminal
                print("El {} ha sido agregado al grupo {}".format(user,my_group))
                # Lo mando a la vista de clientes
                return redirect("/gimnasio/cliente/clienteindex")
            
        data["form"] = formulario
    # cargamos plantilla de registro
    return render(request,"registration/registro.html", data)


def horas(request):
    template = loader.get_template('gimnasio/reserva/horas.html')
    a = datetime.today().strftime('%Y-%m-%d')
    horario = Horario.objects.all().filter(fk_usuario_id=request.user.id).filter(fecha__gte=a)
    usur = historial.objects.filter(usuario=request.user.id).filter(estado=True)

    context = {
        'horario': horario,
        'settings': settings,
        'usur' : usur,
    }

    return HttpResponse(template.render(context, request))
    
    
@user_passes_test(es_cliente)
def delete_horas(request, id):
    horar = Horario.objects.get(id=id)
    print(horar)
    if (request.method == 'GET'):
        form = ReservaDeleteCreationForm(instance=horar)
        print(form)
        # muesta usuarios de esos grupos.
        form.fields['fk_bloque'].queryset = Bloque.objects.filter()
        form.fields['fk_actividad'].queryset = Actividad.objects.filter()
        form.fields['fk_profesor'].queryset = Profesor.objects.filter()
        form.fields['fk_usuario'].queryset = Usuario.objects.filter(id=-1)
        #form.fields['fk_usuario'].queryset = Usuario.objects.filter()
        #queryset = Usuario.objects.filter(groups__name__in=['Investigador', 'Director'])

        # mostrar formulario.
        template = loader.get_template('gimnasio/reserva/delete.html')

        context = {
            'settings': settings,
            'form': form,
            'isadd': False,
        }

        return HttpResponse(template.render(context, request))

    elif (request.method == 'POST'):
        # guardar datos.
        form = ReservaCreationForm(request.POST, instance=horar)
        if form.is_valid():
            print("oli")
            form.save()
            messages.success(request, "Bloque Reservado Eliminado.")
        
        return redirect('../')


