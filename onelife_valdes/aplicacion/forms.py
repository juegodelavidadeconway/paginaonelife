from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Usuario, Horario, Actividad, Profesor, Bloque, Membresia, historial

###
class UsuarioCreationForm(UserCreationForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

class UsuarioChangeForm(UserChangeForm):
    class Meta:
        model = Usuario
        fields = ("username", "email")

class HorarioCreationForm(forms.ModelForm):
    class Meta:
        model = Horario
        #fields = ("fk_bloque", "fecha", "fk_profesor", "fk_actividad", "fk_usuario", 'asistencia')
        fields = ("fk_bloque", "fecha", "fk_profesor", "fk_actividad", "fk_usuario", 'asistencia')
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'fk_bloque': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'fecha': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': True}),
            'fk_profesor': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'fk_actividad': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'fk_usuario': forms.Select(attrs={'class': 'form-control', 'required': False}),
            'asistencia': forms.CheckboxInput(attrs={'type': 'checkbox', 'class': 'checkbox', 'id': 'your_id'}),
        }
        


class CreacionUsuario(forms.ModelForm):
    class Meta:
        model = Usuario
        #fields = ("fk_bloque", "fecha", "fk_profesor", "fk_actividad", "fk_usuario", 'asistencia')
        fields = ("username", "first_name", "last_name", "telefono", "rut", "email")

class HistorialForm(forms.ModelForm):
    class Meta:
        model = historial
        #fields = ("fk_bloque", "fecha", "fk_profesor", "fk_actividad", "fk_usuario", 'asistencia')
        fields = ("membresia", "usuario", "fecha_inicio", "fecha_termino", "estado")
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/
        widgets = {
            'membresia': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'usuario': forms.Select(attrs={'class': 'form-control', 'required': True}),
            'fecha_inicio': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': True}),
            'fecha_termino': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'required': True}),
            'estado': forms.CheckboxInput(attrs={'type': 'checkbox', 'class': 'checkbox', 'id': 'your_id'}),
        }
        
class ReservaCreationForm(forms.ModelForm):
    class Meta:
        model = Horario
        #fields = ("fk_bloque", "fecha", "fk_profesor", "fk_actividad", "fk_usuario", 'asistencia')
        fields = ("fk_bloque", "fecha", "fk_profesor", "fk_actividad", "fk_usuario")
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/

        #con true limita como debe pero no hace ingreso y con falso no limita pero realiza ingreso de datos
        widgets = {
            'fk_bloque': forms.Select(attrs={'class': 'form-control', 'readonly': True}),
            'fecha': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'readonly': True}),
            'fk_profesor': forms.Select(attrs={'class': 'form-control', 'readonly': True}),
            'fk_actividad': forms.Select(attrs={'class': 'form-control', 'readonly': True}),
            'fk_usuario': forms.Select(attrs={'class': 'form-control', 'required': True}),
        }

class ReservaDeleteCreationForm(forms.ModelForm):
    class Meta:
        model = Horario
        #fields = ("fk_bloque", "fecha", "fk_profesor", "fk_actividad", "fk_usuario", 'asistencia')
        fields = ("fk_bloque", "fecha", "fk_profesor", "fk_actividad", "fk_usuario")
        
        #https://docs.djangoproject.com/en/4.0/ref/forms/widgets/

        #con true limita como debe pero no hace ingreso y con falso no limita pero realiza ingreso de datos
        widgets = {
            'fk_bloque': forms.Select(attrs={'class': 'form-control', 'readonly': True}),
            'fecha': forms.DateInput(attrs={'type': 'date', 'class': 'form-control', 'readonly': True}),
            'fk_profesor': forms.Select(attrs={'class': 'form-control', 'readonly': True}),
            'fk_actividad': forms.Select(attrs={'class': 'form-control', 'readonly': True}),
            'fk_usuario': forms.Select(attrs={'class': 'form-control', 'required': False}),
        }       


class RegisterForm(UserCreationForm):
    email = forms.EmailField(
         label= 'Correo Electrónico',
         required=True,
     )
    telefono = forms.IntegerField(
        label = "Ingrese su Numero",
        required=True,
    )
    rut = forms.IntegerField(
        label = "Ingrese su Rut",
        required=True,
    )
    
    username = forms.CharField(
         label = 'Nombre de Usuario',
         required=True,
     )
    first_name = forms.CharField(
        label = "Ingrese su Nombre",
        required=True,
    
    )
    last_name = forms.CharField(
        label = "Ingrese su Apellido",
        required=True,
    )
    password1 = forms.CharField(
         label = "Contraseña",
         required=True,
         widget=forms.PasswordInput
     )
    password2 = forms.CharField(
         label = "Confirmar Contraseña",
         required=True,
         widget=forms.PasswordInput
    )
    class Meta:
        model = Usuario
        fields = ['username','first_name', 'last_name', 'email','telefono', 'password1', 'password2']


